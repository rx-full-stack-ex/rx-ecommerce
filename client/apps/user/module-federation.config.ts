import { ModuleFederationConfig } from '@nx/webpack';

const config: ModuleFederationConfig = {
  name: 'user',
  exposes: {
    './Routes': 'client/apps/user/src/app/remote-entry/entry.routes.ts',
  },
};

export default config;
