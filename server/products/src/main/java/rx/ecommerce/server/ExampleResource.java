package rx.ecommerce.server;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.Operation;

@Path("/hello")
public class ExampleResource {

  @GET
  @Produces(MediaType.TEXT_PLAIN)
  @Operation(summary = "Hello from Quarkus REST")
  public String hello() {
    return "Hello from Quarkus REST";
  }
}
